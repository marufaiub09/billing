-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 01:35 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `billing`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_access`
--

CREATE TABLE IF NOT EXISTS `admin_access` (
`admin_id` int(11) NOT NULL,
  `admin_name` varchar(25) NOT NULL,
  `admin_email` varchar(25) NOT NULL,
  `admin_password` varchar(200) NOT NULL,
  `admin_phone` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `joining_date` varchar(20) NOT NULL,
  `dept` varchar(20) NOT NULL,
  `join_by` varchar(25) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=304765 ;

--
-- Dumping data for table `admin_access`
--

INSERT INTO `admin_access` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_phone`, `status`, `joining_date`, `dept`, `join_by`) VALUES
(1, 'Maruf', 'marufaiub09@gmail.com', '1f16a855141a0e1728937d79960988c9:20L40FV1CH790QNG8IP4YU7P5HGY7HDZ', '', '', '', '', ''),
(304762, 'admin', 'shovon@g.com', '45d433bf442ee9f94426f84ff000f6df:29KEWM5LR1F24TGH993J8P32B3XP1A7F', '01919191293283', 'Active', '2016/03/15', '', 'marufaiub09@gmail.com'),
(304763, 'Nazmul', 'asd@asd.com', '79e57a5640d6a9b14beea4f41700a854:5B239MIZ5T4I06H2HYTIX34K7OHGL495', '01933678178', 'Active', '2016/03/15', 'sales', 'marufaiub09@gmail.com'),
(304764, 'qwq', 'asd@qwe.com', '0951dec8182930bbf931567a1ef3639a:21PCGC0TL1E8505I46CUURKA383788PU', '019293929499', 'Active', '2016/03/15', 'sales', 'marufaiub09@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE IF NOT EXISTS `picture` (
`pic_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `pic_url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(11) NOT NULL,
  `p_code` varchar(80) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `cat_id` varchar(11) NOT NULL,
  `p_buy_price` varchar(50) NOT NULL,
  `p_sell_price` varchar(50) NOT NULL,
  `p_add_by` varchar(50) NOT NULL,
  `p_adding_date` varchar(50) NOT NULL,
  `p_feature` varchar(500) NOT NULL,
  `p_specification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `p_image`
--

CREATE TABLE IF NOT EXISTS `p_image` (
`id` int(11) NOT NULL,
  `p_code` varchar(50) NOT NULL,
  `p_cat_id` int(11) NOT NULL,
  `p_image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_access`
--
ALTER TABLE `admin_access`
 ADD PRIMARY KEY (`admin_id`,`admin_email`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
 ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`,`p_code`);

--
-- Indexes for table `p_image`
--
ALTER TABLE `p_image`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_access`
--
ALTER TABLE `admin_access`
MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=304765;
--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_image`
--
ALTER TABLE `p_image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
