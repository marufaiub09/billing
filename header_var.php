<div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <?php

      include 'connection.php';
      $con = new connection();
      $db  = $con->connect_db();
      $admin_id = $_SESSION['user_id'];
      $query="SELECT * FROM admin_access WHERE admin_email='$admin_id'";
      $result = mysqli_query($db,$query);

      while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
          $admin_name = $data['admin_name'];
          $admin_id = $data['admin_id'];

      }
      ?>
      
      <div class="logo" style="display:block"><span class="theme_color"><?php echo $admin_name; ?></span></div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <div class="top_left">
        <div class="top_left_menu">
          <ul>
            <li> <a href="javascript:void(0);"><i class="fa fa-repeat"></i></a> </li>
            <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0);"> <i class="fa fa-th-large"></i> </a>
			<ul class="drop_down_task dropdown-menu" style="margin-top:39px">
				<div class="top_left_pointer"></div>
				<li><div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember">
                    Remember me </label>
                </div></li>
				<li> <a href="help.html"><i class="fa fa-question-circle"></i> Help</a> </li>
				<li> <a href="settings.html"><i class="fa fa-cog"></i> Setting </a></li>
				<li> <a href="login.html"><i class="fa fa-power-off"></i> Logout</a> </li>
		  </ul>
			</li>
          </ul>
        </div>
      </div>
      <a href="http://bddevteam.com" class="add_user"> <i class="fa fa-plus-square"></i> <span>FreeBirdsBD</span> </a>
      <div class="top_right_bar">
        
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><span class="user_adminname"><?php echo $admin_name;?></span> <b class="caret"></b> </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
            <li> <a href="#"><i class="fa fa-cog"></i> Setting </a></li>
            <li> <a href="logout.php"><i class="fa fa-power-off"></i> Logout</a> </li>
          </ul>
        </div>

        
        
      </div>
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>